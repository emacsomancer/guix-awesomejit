;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2015-2021, 2023, 2024 Efraim Flashner <efraim@flashner.co.il>
;;; Copyright © 2016 Mckinley Olsen <mck.olsen@gmail.com>
;;; Copyright © 2016, 2017, 2019 Alex Griffin <a@ajgrf.com>
;;; Copyright © 2016 David Craven <david@craven.ch>
;;; Copyright © 2016, 2017, 2019, 2020 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2016, 2017 José Miguel Sánchez García <jmi2k@openmailbox.org>
;;; Copyright © 2017–2022 Tobias Geerinckx-Rice <me@tobias.gr>
;;; Copyright © 2017 Kei Kebreau <kkebreau@posteo.net>
;;; Copyright © 2017, 2018, 2019 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2017 Petter <petter@mykolab.ch>
;;; Copyright © 2018 Hartmut Goebel <h.goebel@crazy-compilers.com>
;;; Copyright © 2018 Arun Isaac <arunisaac@systemreboot.net>
;;; Copyright © 2018 Gabriel Hondet <gabrielhondet@gmail.com>
;;; Copyright © 2019 Rutger Helling <rhelling@mykolab.com>
;;; Copyright © 2018, 2019, 2021 Eric Bavier <bavier@posteo.net>
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;; Copyright © 2019, 2021 Pierre Langlois <pierre.langlois@gmx.com>
;;; Copyright © 2019, 2020 Brett Gilio <brettg@gnu.org>
;;; Copyright © 2020 Jakub Kądziołka <kuba@kadziolka.net>
;;; Copyright © 2020 Valentin Ignatev <valentignatev@gmail.com>
;;; Copyright © 2020 Michael Rohleder <mike@rohleder.de>
;;; Copyright © 2020, 2021 Marius Bakke <marius@gnu.org>
;;; Copyright © 2020, 2021 Nicolas Goaziou <mail@nicolasgoaziou.fr>
;;; Copyright © 2020 Leo Famulari <leo@famulari.name>
;;; Copyright © 2020 luhux <luhux@outlook.com>
;;; Copyright © 2021 Ekaitz Zarraga <ekaitz@elenq.tech>
;;; Copyright © 2021, 2022, 2024 Raphaël Mélotte <raphael.melotte@mind.be>
;;; Copyright © 2021 ikasero <ahmed@ikasero.com>
;;; Copyright © 2021 Brice Waegeneire <brice@waegenei.re>
;;; Copyright © 2021 Solene Rapenne <solene@perso.pw>
;;; Copyright © 2021 Petr Hodina <phodina@protonmail.com>
;;; Copyright © 2021 Stefan Reichör <stefan@xsteve.at>
;;; Copyright © 2022 Felipe Balbi <balbi@kernel.org>
;;; Copyright © 2022 ( <paren@disroot.org>
;;; Copyright © 2022, 2023 jgart <jgart@dismail.de>
;;; Copyright © 2023 Aaron Covrig <aaron.covrig.us@ieee.org>
;;; Copyright © 2023 Foundation Devices, Inc. <hello@foundationdevices.com>
;;; Copyright © 2023, 2024 Zheng Junjie <873216071@qq.com>
;;; Copyright © 2023 Jaeme Sifat <jaeme@runbox.com>
;;; Copyright © 2024 Suhail <suhail@bayesians.ca>
;;; Copyright © 2024 Clément Lassieur <clement@lassieur.org>
;;; Copyright © 2024 Ashish SHUKLA <ashish.is@lostca.se>
;;; Copyright © 2024 Ashvith Shetty <ashvithshetty10@gmail.com>
;;; Copyright © 2024 Artyom V. Poptsov <poptsov.artyom@gmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (awesomejit packages terminals)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system go)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system waf)
  #:use-module (guix build-system zig)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages assembly)  
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages dlang)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-build)
  #:use-module (gnu packages golang-xyz)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages man)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages perl-check)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages digest)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages zig)
  #:use-module (awesomejit packages fonts)
  #:use-module (srfi srfi-26))


;; (define-public kitty
;;   (package
;;     (name "kitty")
;;     (version "0.38.0")
;;     (home-page "https://sw.kovidgoyal.net/kitty/")
;;     (source
;;      (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/kovidgoyal/kitty")
;;              (commit (string-append "v" version))))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "082j4mpmi57dj766izilj5zbk0fa4p8b6g9fgvffhd9li98nhi2x"))
;;        ;; (patches (search-patches "kitty-fix-wayland-protocols.patch"))
;;        (modules '((guix build utils)))
;;        (snippet
;;         '(begin
;;            ;; patch needed as sphinx-build is used as a python script
;;            ;; whereas the guix package uses a bash script launching the
;;            ;; python script
;;            (substitute* "docs/conf.py"
;;              (("(from kitty.constants import str_version)" kitty-imp)
;;               (string-append "sys.path.append(\"..\")\n" kitty-imp)))
;;            (substitute* "docs/Makefile"
;;              (("^SPHINXBUILD[[:space:]]+= (python3.*)$")
;;               "SPHINXBUILD = sphinx-build\n"))
;;            #t))))
;;     (build-system gnu-build-system)
;;     (native-inputs
;;      (list dbus
;;            fontconfig
;;            go
;;            golang-std
;;            mesa
;;            libxcursor
;;            libxi
;;            libxinerama
;;            libxkbcommon
;;            libxrandr
;;            ncurses ;; for tic command
;;            font-nerd-fonts-symbols ;; needed font
;;            pkg-config
;;            python-sphinx
;;            openssl ;; libcrypto
;;            xxhash ;; libxxhash
;;            simde 
;;            wayland-protocols))
;;     (inputs
;;      (list fontconfig
;;            freetype
;;            harfbuzz
;;            lcms
;;            libcanberra
;;            libpng
;;            python-pygments
;;            python-wrapper
;;            wayland
;;            zlib))
;;     (arguments
;;      (list
;;       #:phases
;;       #~(modify-phases %standard-phases

;;           (add-before 'build 'set-HOME
;;             ;; The build spams ‘Fontconfig error: No writable cache
;;             ;; directories’ in a seemingly endless loop otherwise.
;;             (lambda _
;;               (setenv "HOME" "/tmp")))
;;           (delete 'configure)   ;no configure script
;;           (replace 'build
;;             (lambda* (#:key inputs #:allow-other-keys)
;;               ;; The "kitty" sub-directory must be writable prior to
;;               ;; configuration (e.g., un-setting updates).
;;               (for-each make-file-writable (find-files "kitty"))
;;               (invoke "python3" "setup.py" "linux-package"
;;                       ;; Do not phone home.
;;                       "--update-check-interval=0"
;;                       ;; Wayland backend requires EGL, which isn't
;;                       ;; found out-of-the-box for some reason.
;;                       (string-append "--egl-library="
;;                                      (search-input-file inputs "/lib/libEGL.so.1")))))
;;           (replace 'check
;;             (lambda* (#:key tests? #:allow-other-keys)
;;               (when tests?
;;                 ;; Fix "cannot find kitty executable" error when running
;;                 ;; tests.
;;                 (setenv "PATH" (string-append "linux-package/bin:"
;;                                               (getenv "PATH")))
;;                 (invoke "python3" "test.py"))))
;;           (add-before 'install 'rm-pycache
;;             ;; created python cache __pycache__ are non deterministic
;;             (lambda _
;;               (let ((pycaches (find-files "linux-package/"
;;                                           "__pycache__"
;;                                           #:directories? #t)))
;;                 (for-each delete-file-recursively pycaches))))
;;           (replace 'install
;;             (lambda _
;;               (let* ((obin (string-append #$output "/bin"))
;;                      (olib (string-append #$output "/lib"))
;;                      (oshare (string-append #$output "/share")))
;;                 (copy-recursively "linux-package/bin" obin)
;;                 (copy-recursively "linux-package/share" oshare)
;;                 (copy-recursively "linux-package/lib" olib)))))))
;;     (synopsis "Fast, featureful, GPU based terminal emulator")
;;     (description "Kitty is a fast and featureful GPU-based terminal emulator:
;; @itemize
;; @item Offloads rendering to the GPU for lower system load and buttery smooth
;; scrolling.  Uses threaded rendering to minimize input latency.
;; @item Supports all modern terminal features: graphics (images), unicode,
;; true-color, OpenType ligatures, mouse protocol, focus tracking, bracketed
;; paste and several new terminal protocol extensions.
;; @item Supports tiling multiple terminal windows side by side in different
;; layouts without needing to use an extra program like tmux.
;; @item Can be controlled from scripts or the shell prompt, even over SSH.
;; @item Has a framework for Kittens, small terminal programs that can be used to
;; extend kitty's functionality.  For example, they are used for Unicode input,
;; hints, and side-by-side diff.
;; @item Supports startup sessions which allow you to specify the window/tab
;; layout, working directories and programs to run on startup.
;; @item Allows you to open the scrollback buffer in a separate window using
;; arbitrary programs of your choice.  This is useful for browsing the history
;; comfortably in a pager or editor.
;; @end itemize")
;;     (license license:gpl3+)))

;; (define-public zutty
;;   (package
;;     (name "zutty")
;;     (version "0.16")
;;     (source
;;      (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://git.hq.sig7.se/zutty.git")
;;              (commit version)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32
;;          "115skr3lcw0hdshgxl72qnh635ajy7kk07dnrh9fhbx1bhvqcm3k"))))
;;     (build-system gnu-build-system)
;;     (arguments
;;      `(#:phases
;;        (modify-phases %standard-phases
;;          (delete 'configure)            ; no configure script
;;        )))
;;     (native-inputs
;;      (list pkg-config python-waf python))
;;     (inputs
;;      (list freetype libxmu glew))
;;     (home-page "https://tomscii.sig7.se/zutty/")
;;     (synopsis "X terminal emulator rendering through OpenGL ES Compute Shaders")
;;     (description "Zutty is a GPU-accelerated terminal emulator for the X Window
;; System, Zutty is written in C++ and only relying on OpenGL ES 3.1 for rendering.
;; What really sets Zutty apart is its radically simple, yet extremely efficient
;; rendering implementation, coupled with a sufficiently complete (VTxxx) feature
;; set to make it useful for a wide range of users. Zutty offers high throughput
;; with low latency, and strives to conform to relevant (published or de-facto)
;; standards. Zutty provides a clean implementation written from scratch, resulting
;; in a minimal, maintainable, modern codebase unencumbered by historical baggage.")
;;     (license license:gpl3)))

(define-public zutty
  (package
    (name "zutty")
    (version "0.16")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.hq.sig7.se/zutty.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "115skr3lcw0hdshgxl72qnh635ajy7kk07dnrh9fhbx1bhvqcm3k"))))
    (build-system waf-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'check)            ; no check
       )))
    ;; (propagated-inputs
    ;;  (list libxmu))
    (native-inputs
     (list pkg-config python))
    (inputs
     (list freetype
           glew
           font-misc-misc ;; zutty has 9x18 font as default
           libxt ;; needed for xmu to be detected by `pkg-config'
           libxmu))
    (home-page "https://tomscii.sig7.se/zutty/")
    (synopsis "X terminal emulator rendering through OpenGL ES Compute Shaders")
    (description "Zutty [zuːc̟] is a GPU-accelerated terminal emulator for the X
Window, written in C++ and only relying on OpenGL ES 3.1 for rendering.
What really sets Zutty apart is its radically simple, yet extremely efficient
rendering implementation, coupled with a sufficiently complete (VTxxx) feature
set to make it useful for a wide range of users. Zutty offers high throughput
with low latency, and strives to conform to relevant (published or de-facto)
standards. Zutty provides a clean implementation written from scratch, resulting
in a minimal, maintainable, modern codebase unencumbered by historical baggage.")
    (license license:gpl3)))

;; (define-public ghostty
;;   (package
;;     (name "ghostty")
;;     (version "1.0.1")
;;     (source
;;      (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/ghostty-org/ghostty")
;;              ;; (commit version)
;;              (commit (string-append "v" version))
;;              ))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "05czkg0f6ixjgabi6w1wa8jklr345crbihmci8lidy0bx8swa986"))))
;;     ;; (source
;;     ;;  (origin
;;     ;;    (method url-fetch)
;;     ;;    (uri (string-append
;;     ;;          "https://github.com/ghostty-org/ghostty/archive/refs/tags/v"
;;     ;;          version ".zip"))
;;     ;;    (sha256
;;     ;;     (base32 "096lmhla6lgdf6hm4a2p4ixj4r82ardr88bndlp8qj6p3kibapjp"))))
      
;;     (build-system zig-build-system)
;;     ;; (arguments
;;     ;;  (list
;;     ;;   #:phases
;;     ;;   #~(modify-phases %standard-phases
;;     ;;       (add-after 'install 'install-wayland-session
;;     ;;         (lambda* (#:key outputs #:allow-other-keys)
;;     ;;           (let* ((out (assoc-ref outputs "out"))
;;     ;;                  (wayland-sessions
;;     ;;                   (string-append out "/share/wayland-sessions")))
;;     ;;             (mkdir-p wayland-sessions)
;;     ;;             (install-file "contrib/river.desktop"
;;     ;;                           wayland-sessions)))))
;;     ;;   #:zig-build-flags #~(list "-Dxwayland") ;experimental xwayland support
;;     ;;   #:zig-release-type "safe"))
;;     (native-inputs (list pandoc
;;                          pkg-config
;;                          glib))
;;     (inputs (list fontconfig
;;                   freetype
;;                   harfbuzz
;;                   gtk
;;                   libadwaita))
;;     (home-page "https://ghostty.org")
;;     (synopsis "GPU-accelerated terminal emulator")
;;     (description
;;      "Ghostty is a terminal emulator that differentiates itself by being
;; fast, feature-rich, and native.")
;;     (license license:expat)))
