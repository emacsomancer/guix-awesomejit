;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2013-2021, 2023 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2014, 2015, 2017, 2020 Mark H Weaver <mhw@netris.org>
;;; Copyright © 2014 Ian Denhardt <ian@zenhack.net>
;;; Copyright © 2014 Sou Bunnbu <iyzsong@gmail.com>
;;; Copyright © 2014, 2019 Julien Lepiller <julien@lepiller.eu>
;;; Copyright © 2015 Taylan Ulrich Bayırlı/Kammer <taylanbayirli@gmail.com>
;;; Copyright © 2015 Paul van der Walt <paul@denknerd.org>
;;; Copyright © 2015, 2016, 2018 Eric Bavier <bavier@member.fsf.org>
;;; Copyright © 2015 Andreas Enge <andreas@enge.fr>
;;; Copyright © 2015-2023 Efraim Flashner <efraim@flashner.co.il>
;;; Copyright © 2016 Christine Lemmer-Webber <cwebber@dustycloud.org>
;;; Copyright © 2016 Al McElrath <hello@yrns.org>
;;; Copyright © 2016, 2017, 2018, 2019, 2020, 2021 Leo Famulari <leo@famulari.name>
;;; Copyright © 2016 Lukas Gradl <lgradl@openmailbox.org>
;;; Copyright © 2016 Alex Kost <alezost@gmail.com>
;;; Copyright © 2016, 2017 Troy Sankey <sankeytms@gmail.com>
;;; Copyright © 2016, 2017, 2018 Nikita <nikita@n0.is>
;;; Copyright © 2016 Clément Lassieur <clement@lassieur.org>
;;; Copyright © 2016–2023 Arun Isaac <arunisaac@systemreboot.net>
;;; Copyright © 2016 John Darrington <jmd@gnu.org>
;;; Copyright © 2016, 2018 Marius Bakke <mbakke@fastmail.com>
;;; Copyright © 2017 Thomas Danckaert <post@thomasdanckaert.be>
;;; Copyright © 2017 Kyle Meyer <kyle@kyleam.com>
;;; Copyright © 2017–2022 Tobias Geerinckx-Rice <me@tobias.gr>
;;; Copyright © 2017, 2018, 2020 Rene Saavedra <pacoon@protonmail.com>
;;; Copyright © 2018, 2019, 2020, 2021, 2022 Pierre Langlois <pierre.langlois@gmx.com>
;;; Copyright © 2018 Alex Vong <alexvong1995@gmail.com>
;;; Copyright © 2018 Gábor Boskovits <boskovits@gmail.com>
;;; Copyright © 2018-2024 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2019–2022 Tanguy Le Carrour <tanguy@bioneland.org>
;;; Copyright © 2020 Vincent Legoll <vincent.legoll@gmail.com>
;;; Copyright © 2020 Justus Winter <justus@sequoia-pgp.org>
;;; Copyright © 2020 Eric Brown <ecbrown@ericcbrown.com>
;;; Copyright © 2020, 2021, 2022, 2024 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;; Copyright © 2020 Michael Rohleder <mike@rohleder.de>
;;; Copyright © 2020, 2021 Alexey Abramov <levenson@mmer.org>
;;; Copyright © 2020 Tim Gesthuizen <tim.gesthuizen@yahoo.de>
;;; Copyright © 2020, 2021 Alexandru-Sergiu Marton <brown121407@posteo.ro>
;;; Copyright © 2020 Oleg Pykhalov <go.wigust@gmail.com>
;;; Copyright © 2020 B. Wilson <elaexuotee@wilsonb.com>
;;; Copyright © 2020 divoplade <d@divoplade.fr>
;;; Copyright © 2021 Xinglu Chen <public@yoctocell.xyz>
;;; Copyright © 2021 Benoit Joly <benoit@benoitj.ca>
;;; Copyright © 2021 Morgan Smith <Morgan.J.Smith@outlook.com>
;;; Copyright © 2021 Philip McGrath <philip@philipmcgrath.com>
;;; Copyright © 2022 Andrew Tropin <andrew@trop.in>
;;; Copyright © 2022 Justin Veilleux <terramorpha@cock.li>
;;; Copyright © 2022 Thiago Jung Bauermann <bauermann@kolabnow.com>
;;; Copyright © 2022 Guillaume Le Vaillant <glv@posteo.net>
;;; Copyright © 2022 muradm <mail@muradm.net>
;;; Copyright © 2022 jgart <jgart@dismail.de>
;;; Copyright © 2022 ( <paren@disroot.org>
;;; Copyright © 2023 Timo Wilken <guix@twilken.net>
;;; Copyright © 2023 Arjan Adriaanse <arjan@adriaan.se>
;;; Copyright © 2023 Wilko Meyer <w@wmeyer.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (awesomejit packages mail)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages aspell)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages cyrus-sasl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages dbm)
  #:use-module (gnu packages dejagnu)
  #:use-module (gnu packages django)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages docker)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages file)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gdb)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-build)
  #:use-module (gnu packages golang-check)
  #:use-module (gnu packages golang-crypto)
  #:use-module (gnu packages golang-web)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages gsasl)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages haskell-xyz)
;;  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages kerberos)
  #:use-module (gnu packages language)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages libidn)
  #:use-module (gnu packages libunistring)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lsof)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages man)
  #:use-module (gnu packages mercury)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages nettle)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages onc-rpc)
  #:use-module (gnu packages openldap)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages perl-check)
  #:use-module (gnu packages perl-web)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages ragel)
  #:use-module (gnu packages rdf)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages regex)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages samba)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages search)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages time)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages w3m)
  #:use-module (gnu packages web)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system guile)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system trivial)
  #:use-module (guix deprecation)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix svn-download)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))


;; (define-public isync
;;   (package
;;     (name "isync")
;;     (version "1.5.0")
;;     (source
;;      (origin
;;        (method url-fetch)
;;        (uri (string-append "mirror://sourceforge/isync/isync/"
;;                            version "/isync-" version ".tar.gz"))
;;        (sha256 (base32
;;                 "13gpawspfpiw87wm2p6gzrm98xwy741k2ib1l6fjggw7jc81xj50"))
;;        ;; (patches
;;        ;;  ;; Likely to be included in next version
;;        ;;  (search-patches "isync-openssl3-fix.patch"))
;;        ))
;;     (build-system gnu-build-system)
;;     (arguments
;;      (list
;;        #:phases
;;        #~(modify-phases %standard-phases
;;            (add-after 'install 'substitute-openssl-path
;;              (lambda* (#:key inputs outputs #:allow-other-keys)
;;                (substitute* (string-append #$output "/bin/mbsync-get-cert")
;;                  (("openssl s_client")
;;                   (string-append (search-input-file inputs "/bin/openssl")
;;                                  " s_client"))))))))
;;     (native-inputs
;;      (list perl))
;;     (inputs
;;      (list bdb cyrus-sasl openssl zlib))
;;     (home-page "https://isync.sourceforge.io/")
;;     (synopsis "Mailbox synchronization program")
;;     (description
;;      "isync/mbsync is a command-line tool for two-way synchronization of
;; mailboxes.  Currently Maildir and IMAP are supported types.")
;;     (license license:gpl2+)))

;; (define-public mu
;;   (package
;;     (name "mu")
;;     (version "1.12.9")
;;     (source
;;      (origin
;;        (method url-fetch)
;;        (uri (string-append "https://github.com/djcb/mu/releases/download/v"
;;                            version "/mu-" version ".tar.xz"))
;;        (sha256
;;         (base32 "1rbdw4w1vwndgi9bc1n07cwb806df21q8j7rm1f82dmmxbs3wjgd"))))
;;     (build-system meson-build-system)
;;     (native-inputs
;;      (list pkg-config
;;            emacs-minimal
;;            gnupg                        ; for tests
;;            texinfo))
;;     (inputs
;;      (list glib gmime guile-3.0 xapian readline python))
;;     (arguments
;;      (list
;;       #:modules '((guix build meson-build-system)
;;                   (guix build emacs-utils)
;;                   (guix build utils))
;;       #:imported-modules `(,@%meson-build-system-modules
;;                            (guix build emacs-utils))
;;       #:configure-flags
;;       #~(list (format #f "-Dguile-extension-dir=~a/lib" #$output))
;;       #:phases
;;       #~(modify-phases %standard-phases
;;           (add-after 'unpack 'patch-bin-references
;;             (lambda _
;;               (substitute* '("guile/tests/test-mu-guile.cc"
;;                              ;; "mu/tests/test-mu-cmd.cc"
;;                              ;; "mu/tests/test-mu-cmd-cfind.cc"
;;                              "mu/tests/test-mu-query.cc")
;;                 (("/bin/sh") (which "sh")))
;;               (substitute* '("lib/tests/bench-indexer.cc"
;;                              "lib/utils/mu-test-utils.cc")
;;                 (("/bin/rm") (which "rm")))
;;               (substitute* '("lib/mu-maildir.cc")
;;                 (("/bin/mv") (which "mv")))))
;;           (add-after 'install 'fix-ffi
;;             (lambda _
;;               (substitute* (find-files #$output "mu.scm")
;;                 (("\"libguile-mu\"")
;;                  (format #f "\"~a/lib/libguile-mu\"" #$output)))))
;;           (add-after 'install 'install-emacs-autoloads
;;             (lambda* (#:key outputs #:allow-other-keys)
;;               (emacs-generate-autoloads
;;                "mu4e"
;;                (string-append (assoc-ref outputs "out")
;;                               "/share/emacs/site-lisp/mu4e")))))))
;;     (home-page "https://www.djcbsoftware.nl/code/mu/")
;;     (synopsis "Quickly find emails")
;;     (description
;;      "Mu is a tool for dealing with e-mail messages stored in the
;; Maildir format.  Mu's purpose in life is to help you to quickly find the
;; messages you need; in addition, it allows you to view messages, extract
;; attachments, create new maildirs, and so on.")
;;     (license license:gpl3+)))
